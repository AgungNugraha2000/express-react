const express = require("express")
const app = express()
const PORT = 8080
const bodyParser = require("body-parser")
global.config = require("./configure/config")
const cors = require("cors")
const multer = require("multer")

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "uploads/")
    },
    filename: function(req, file, cb) {
        cb(null, `${file.originalname}`)
    }
})

const upload = multer({ storage: storage })
var typeUpload = upload.single("image")

app.use(express.static('uploads'))

// untuk membaca body yang dikirim melalui method post
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extends: true }))

const corsOptions = {
    origin: ["http://localhost:3000", "http://localhost:3111", '*']
}
app.use(cors(corsOptions))

// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*")
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
//     res.header("Access-Control-Allow-Method", "GET, POST, PUT, DELETE")
//     next()
// })

require("./service/categoryService")(app, global.config.pool)
require("./service/variantService")(app, global.config.pool)
require("./service/productService")(app, global.config.pool, typeUpload)
require("./service/transactionService")(app, global.config.pool)
require("./service/authService")(app, global.config.pool)

app.get("/halo/:id", (req, res) => {
    const id = req.params.id
    
    res.send(200 , {
        biodata: {
            id: id,
            name: "peter",
            alamat: "Jakarta"
        }
    })
})

app.post("/datapost", (req, res) => {
    // console.log(req.body)

    res.send("data success to send")
})

app.listen(PORT)