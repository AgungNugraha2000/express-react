module.exports = exports = (app, pool) => {
  app.post("/api/signup", (req, res) => {
    const { email, password, name_customer, no_hp, gender, address } = req.body;

    let queryDetails = "";

    queryDetails += queryDetails.length > 0 ? ", " : "";
    queryDetails += `(
                        ( select "id_user" from "InsertUser"), 
                          '${name_customer}', '${no_hp}', '${gender}', '${address}', 
                          false, 1, now()
                      )`;

    // console.log(queryDetails);

    const query = `WITH "InsertUser" AS(
                INSERT INTO "TblUser" 
                      (email, password, is_delete, create_by, create_date)
                  VALUES ('${email}','${password}', false, 1, now())
                RETURNING "id_user"),
                "InsertCustomer" AS ( 
                INSERT INTO "TblCustomer"(
                      id_user, name_customer, no_hp, gender, address, is_delete, create_by, create_date)
                  VALUES
                  ${queryDetails}
                ) SELECT * FROM "InsertUser"`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(201, {
          success: true,
          result: "Congratulation success registered",
        });
      }
    });
  });

  app.post("/api/signin", (req, res) => {
    const {email, password} = req.body;
    // console.log(email, '== email')
    // console.log(password)

    const query = `select
                    u.id_user,
                    c.name_customer,
                    c.id_customer
                    from "TblCustomer" c
                    join "TblUser" u on u.id_user = c.id_user
                    where u.email = '${email}' AND u.password = '${password}' `;

    pool.query(query, (error, result) => {
      // const user = result.rows[0];
      // console.log(user.email, '== email user')
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
          message: 'success login'
        });
      }
    });
  });

  app.post("/api/authvalidate", (req, res) => {
    const {email, password} = req.body;

    const query = `SELECT COUNT (1) AS bit from "TblUser" WHERE email = '${email}' AND password = '${password}'`

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error
        })
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
          message: 'Email atau Password tidak sesuai'
        })
      }
    })
  })
};
