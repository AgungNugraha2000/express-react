module.exports = exports = (app, pool) => {
  app.get("/api/allvariant", (req, res) => {
    const query = `SELECT 
                    v."id", v."name_variant", v."description",
                    v."category_id", c."name_category", c."description" as  "DescriptionCategory"
                    FROM "TblVariant" AS v
                    JOIN "TblCategory" as c 
                    ON v."category_id" = c."id"
                    WHERE v."is_delete" = false ORDER BY v."id" ASC`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.get("/api/variantby/:id", (req, res) => {
    const id = req.params.id;

    const query = `SELECT 
                    v."id", v."name_variant", v."description",
                    v."category_id", c."name_category", c."description" as  "DescriptionCategory"
                    FROM "TblVariant" AS v
                    JOIN "TblCategory" as c 
                    ON v."category_id" = c."id"
                    WHERE v."is_delete" = false AND v."id" = ${id}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
        });
      }
    });
  });

  app.post("/api/allvariantbyfilter", (req, res) => {
    const { search, sort, page, pageSize } = req.body;
    const qFilter =
      search !== "" ? `AND "name_variant" ilike '%${search}%'` : "";
    const qSort =
      sort !== "" ? `ORDER BY "name_variant" ${sort}` : `ORDER BY "id"`;
    const qPage = search === "" ? (page - 1) * pageSize : 0;

    const query = `SELECT 
                  v."id", v."name_variant", v."description",
                  v."category_id", c."name_category", c."description" as "DescriptionCategory"
                  FROM "TblVariant" AS v
                  JOIN "TblCategory" as c 
                  ON v."category_id" = c."id" 
                  WHERE v."is_delete" = false ${qFilter} ${qSort} LIMIT ${pageSize} OFFSET ${qPage}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.post("/api/countvariant", (req, res) => {
    const { search } = req.body;
    const qFilter = search !== "" ? `AND "name_variant" ilike '%${search}%'` : "";
    const query = `SELECT COUNT(*) FROM "TblVariant" WHERE "is_delete" = false ${qFilter}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
        });
      }
    });
  });

  app.post("/api/addvariant", (req, res) => {
    const { category_id, name_variant, description, create_by } = req.body;
    // console.log(req.body)
    const query = `INSERT INTO public."TblVariant"(
            category_id, name_variant, description, create_by, create_date, is_delete)
            VALUES (${category_id}, '${name_variant}', '${description}', ${create_by}, now(), false);`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(201, {
          success: true,
          result: `Data ${name_variant} been saved`,
        });
      }
    });
  });

  app.put("/api/updatevariant/:id", (req, res) => {
    const id = req.params.id;
    const { category_id, name_variant, description, update_by } =
      req.body;
    console.log(req.body);

    const query = `UPDATE public."TblVariant"
        SET category_id=${category_id}, name_variant='${name_variant}', description='${description}', update_by=${update_by}, update_date=now()
        WHERE id = ${id};`;

    // console.log(query)

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: `Data with id ${name_variant} success updated`,
        });
      }
    });
  });

  app.put("/api/deletevariant/:id", (req, res) => {
    const id = req.params.id;
    const { update_by } = req.body;

    const query = `UPDATE public."TblVariant"
        SET update_by=${update_by}, update_date=now(), is_delete=true
        WHERE id = ${id};`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: `Data with id ${id} is deleted`,
        });
      }
    });
  });

  app.delete("/api/deletedvariant/:id", (req, res) => {
    const id = req.params.id;
    const query = `DELETE FROM public."TblVariant"
        WHERE id = ${id};`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: `Data id ${id} is deleted`,
        });
      }
    });
  });
};
