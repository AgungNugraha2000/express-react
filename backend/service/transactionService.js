module.exports = exports = (app, pool) => {
  // app.get("/api/historyorder/:id", (req, res) => {
  //   const id = req.params.id;
  //   const { code_transaction } = req.body;

  //   const query = `select
  //                   p.id,
  //                   p.name_product,
  //                   p.price,
  //                   p.stock,
  //                   oh.id_order_header,
  //                   oh.code_transaction,
  //                   oh.id_customer,
  //                   oh.total_qty,
  //                   oh.total_amount,
  //                   oh.is_checkout,
  //                   oh.create_date AS date_header,
  //                   od.id_order_detail,
  //                   od.qty,
  //                   od.amount,
  //                   od.id_order_header,
  //                   od.id_product,
  //                   od.create_date AS date_detail
  //                   from "TblOrderDetail" od
  //                   join "TblOrderHeader" oh on oh.id_order_header = od.id_order_header
  //                   join "TblProduct" p on p.id = od.id_product
  //                   where oh.id_customer = ${id}`;

  //   pool.query(query, (error, result) => {
  //     if (error) {
  //       res.send(500, {
  //         success: false,
  //         result: error,
  //       });
  //     } else {
  //       res.send(200, {
  //         success: true,
  //         result: result.rows,
  //       });
  //     }
  //   });
  // });

  app.get("/api/getheader", (req, res) => {
    const query = `SELECT * FROM "TblOrderHeader"
    ORDER BY id_order_header ASC `;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.get("/api/getdetail/:id", (req, res) => {
    const id = req.params.id

    const query = `select
                    p."id",
                    p."name_product",
                    p."price",
                    oh."id_order_header",
                    oh."code_transaction",
                    oh."id_customer",
                    oh."total_qty",
                    oh."total_amount",
                    oh."is_checkout",
                    oh."create_date" AS date_header,
                    od."id_order_detail",
                    od."qty",
                    od."amount",
                    od."id_order_header",
                    od."id_product",
                    od."create_date" AS date_detail
                    from "TblOrderDetail" od
                    join "TblOrderHeader" oh on oh."id_order_header" = od."id_order_header"
                    join "TblProduct" p on p.id = od."id_product"
                    where oh."id_order_header" = ${id}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.post("/api/orderpayment", (req, res) => {
    const { header, detail } = req.body;
    console.log(header);
    console.log(detail);

    getCodeTransaction((code) => {
      // console.log("hasil return ", code);

      let queryDetails = "";
      let queryProducts = "";
      detail.forEach((data) => {
        queryDetails += queryDetails.length > 0 ? "," : "";
        queryDetails += `(
                            ( select "id_order_header" from "InsertHeader"), 
                            ${data.Qty}, ${data.Amount}, ${data.productId},
                            false, 1, now()
                            )`;

        queryProducts += queryProducts.length > 0 ? "," : "";
        queryProducts += `"UpdatedProduct${data.productId}" AS (
          UPDATE "TblProduct" set "stock" = "stock" - ${data.Qty} WHERE "id" = ${data.productId}
        )`;
      });
      // console.log(queryDetails);

      const query = `WITH "InsertHeader" AS(
                             INSERT INTO "TblOrderHeader" 
                                ("code_transaction", "id_customer", "total_qty", "total_amount", 
                                "is_checkout", "is_delete", "create_by", "create_date")
                             VALUES ('${code}', 1, ${header.TotalProduct}, ${header.EstimatePrice},
                                true, false, 1, now())
                             RETURNING "id_order_header"),
                                "InsertDetail" AS ( 
                            INSERT INTO "TblOrderDetail"(
                                "id_order_header" , "qty" , "amount",  "id_product", "is_delete", "create_by", "create_date")
                             VALUES
                                ${queryDetails}
                            ), ${queryProducts} SELECT * FROM "InsertHeader"`;

      // console.log(query);

      pool.query(query, (error, result) => {
        if (error) {
          res.send(500, {
            success: false,
            result: error,
          });
        } else {
          res.send(200, {
            success: true,
            result: result.rows,
            message: `Data Ordered`,
          });
        }
      });
    });
  });

  function getCodeTransaction(callback) {
    let code = "XPOS-";
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear().toString();

    let formatdate =
      (day < 10 ? "0" + day.toString() : day.toString()) +
      (month < 10 ? "0" + month.toString() : month.toString()) +
      year;

    const query = `SELECT "code_transaction" FROM "TblOrderHeader" ORDER BY "code_transaction" DESC LIMIT 1`;
    // console.log(query)

    let hasil = {};
    pool.query(query, (error, result) => {
      if (error) {
        return {
          success: false,
          result: error,
        };
      } else {
        if (result.rows.length === 0) {
          return callback(code + formatdate + "-00001");
        } else {
          let lastcode = result.rows[0].code_transaction;
          let arrayCode = lastcode.split("-");
          let digitInc = parseInt(arrayCode[2]) + 1;
          //XPOS-01032023-
          let newDigit = ("00000" + digitInc.toString()).slice(-5);
          return callback(code + formatdate + "-" + newDigit);
        }
      }
    });
    // console.log(hasil);
    return code + formatdate + hasil.result;
  }
};
