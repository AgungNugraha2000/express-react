module.exports = exports = (app, pool) => {

    app.get('/api/allcategory', (req, res) => {
        const query = `SELECT * FROM "TblCategory" WHERE "isDelete" = false ORDER BY "id" ASC`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })

    })

    app.get('/api/categoryby/:id', (req, res) => {
        const id = req.params.id

        const query = `SELECT * FROM "TblCategory" WHERE id = ${id} AND "isDelete" = false;`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })
    })

    app.post('/api/allcategorybyfilter', (req, res) => {
        const { search, sort, page, pageSize } = req.body
        const qFilter = search !== '' ? `AND "name_category" ilike '%${search}%'` : ""
        const qSort = sort !== '' ? `ORDER BY "name_category" ${sort}` : `ORDER BY "id"`
        const qPage = search === '' ? (page - 1) * pageSize : 0
        
        const query = `SELECT * FROM "TblCategory" WHERE "isDelete" = false ${qFilter} ${qSort} LIMIT ${pageSize} OFFSET ${qPage}`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })

    app.post('/api/countcategory', (req, res) => {
        const { search } = req.body
        const qFilter = search !== '' ? `AND "name_category" ilike '%${search}%'` : ""
        const query = `SELECT COUNT(*) FROM "TblCategory" WHERE "isDelete" = false ${qFilter}`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })

    })

    app.post('/api/addcategory', (req, res) => {
        // console.log(req.body)
        const {name_category, description, create_by} = req.body

        const query = `INSERT INTO public."TblCategory"(
            name_category, description, create_by, create_date, "isDelete")
            VALUES ('${name_category}', '${description}', '${create_by}', now(), false);`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(201, {
                    success: true,
                    result: `Data ${name_category} success saved`
                })
            }
        })
    })

    app.put('/api/updatecategory/:id', (req, res) => {
        const id = req.params.id
        const {name_category, description, update_by} = req.body

        const query = `UPDATE public."TblCategory"
        SET name_category='${name_category}', description='${description}', update_by=${update_by}, update_date=now()
        WHERE id = ${id};`
        // console.log(query)

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${name_category} success updated`
                })
            }
        })
    })

    app.put('/api/deletecategory/:id', (req, res) => {
        const id = req.params.id
        const {name_category, update_by} = req.body
        
        const query = `UPDATE public."TblCategory"
        SET update_by=${update_by}, update_date=now(), "isDelete"=true
        WHERE id = ${id};`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${name_category} is deleted`
                })
            }
        })
    })

    app.delete('/api/deletedcategory/:id', (req, res) => {
        const id = req.params.id

        const query = `DELETE FROM public."TblCategory"
        WHERE id = ${id};`

        pool.query(query, (error, result) => {
            if (error) {
                res.send(500, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data dengan id ${id} telah dihapus`
                })
            }
        })
    })

}