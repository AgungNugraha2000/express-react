module.exports = exports = (app, pool, typeUpload) => {
  app.get("/api/allproduct", (req, res) => {
    // const query = `SELECT
    //     p."id", p."name_product", p."price", p."stock", p."variant_id", p."image", v."name_variant", c."name_category"
    //     FROM "TblProduct" AS p
    //     JOIN "TblVariant" AS v ON p."variant_id" = v."id"
    //     JOIN "TblCategory AS c ON v."category_id" = c."id"
    //     WHERE p."is_delete" = false ORDER BY p."id" ASC`;

    const query = `SELECT
                    c."name_category",
                    v."name_variant",
                    p."id",
                    p."name_product",
                    p."price",
                    p."stock",
                    p."image"
                    FROM "TblProduct" AS p
                    JOIN "TblVariant" AS v ON v."id" = p."variant_id"
                    JOIN "TblCategory" AS c ON c."id" = v."category_id"
                    WHERE p."is_delete" = false ORDER BY p."id" ASC`

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.get("/api/productby/:id", (req, res) => {
    const id = req.params.id;

    const query = `SELECT
        c."name_category",
        v."name_variant",
        p."id",
        p."name_product",
        p."variant_id",
        p."price",
        p."stock",
        p."image"
        FROM "TblProduct" AS p
        JOIN "TblVariant" AS v ON v."id" = p."variant_id"
        JOIN "TblCategory" AS c ON c."id" = v."category_id"
        WHERE p."is_delete" = false AND p."id" = ${id}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
        });
      }
    });
  });

  app.get("/api/allvariantbyidcategory/:id", (req, res) => {
    const id = req.params.id;
    const query = `SELECT
                  c."name_category",
                  c."isDelete",
                  v."id",
                  v."category_id",
                  v."name_variant"
                  FROM "TblVariant" AS v
                  JOIN "TblCategory" AS c ON c."id" = v."category_id"
                  WHERE v."category_id" = ${id}`;

    // console.log(query);

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.post("/api/allproductbyfilter", (req, res) => {
    const { search, sort, page, pageSize } = req.body;
    const qFilter =
      search !== "" ? `AND "name_product" ilike '%${search}%'` : "";
    const qSort =
      sort !== "" ? `ORDER BY "name_product" ${sort}` : `ORDER BY "id"`;
    const qPage = search === "" ? (page - 1) * pageSize : 0;

    // const query = `SELECT
    //               p."id", p."name_product", p."price", p."stock", p."variant_id", p."image", v."name_variant"
    //               FROM "TblProduct" AS p
    //               JOIN "TblVariant" AS v ON p."variant_id" = v."id"
    //               WHERE p."is_delete" = false ${qFilter} ${qSort} LIMIT ${pageSize} OFFSET ${qPage}`;
    const query = `SELECT
                  c."name_category",
                  v."name_variant",
                  p."id",
                  p."name_product",
                  p."price",
                  p."stock",
                  p."image"
                  FROM "TblProduct" AS p
                  JOIN "TblVariant" AS v ON v."id" = p."variant_id"
                  JOIN "TblCategory" AS c ON c."id" = v."category_id"
                  WHERE p."is_delete" = false ${qFilter} ${qSort} LIMIT ${pageSize} OFFSET ${qPage}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows,
        });
      }
    });
  });

  app.post("/api/countproduct", (req, res) => {
    const { search } = req.body;
    const qFilter =
      search !== "" ? `AND "name_product" ilike '%${search}%'` : "";

    const query = `SELECT COUNT(*) FROM "TblProduct" WHERE "is_delete" = false ${qFilter}`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: result.rows[0],
        });
      }
    });
  });

  app.post("/api/addproduct", (req, res) => {
    const { name_product, price, stock, variant_id, image, create_by } =
      req.body;

    const query = `INSERT INTO public."TblProduct"(
        name_product, price, stock, variant_id, image, create_by, create_date, is_delete)
        VALUES ('${name_product}', ${price}, ${stock}, ${variant_id}, '${image}', ${create_by}, now(), false);`;

    console.log(query)
    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(201, {
          success: true,
          result: `Data with name ${name_product} been saved`,
        });
      }
    });
  });

  app.post("/api/upload", typeUpload, function (req, res) {
    console.log("upload", req.file);
    res.send("file saved on server");
  });

  app.put("/api/updateproduct/:id", (req, res) => {
    const id = req.params.id;
    const { name_product, price, stock, variant_id, image, update_by } = req.body;

    const query = `UPDATE public."TblProduct"
                    SET name_product='${name_product}', price=${price}, stock=${stock}, variant_id=${variant_id}, image='${image}' ,update_by=${update_by}, update_date=now()
                    WHERE id = ${id};`;

  console.log(query)

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: `Data with ${name_product} success updated`,
        });
      }
    });
  });

  app.put("/deleteproduct/:id", (req, res) => {
    const id = req.params.id;
    const { update_by } = req.body;

    const query = `UPDATE public."TblProduct"
	SET update_by=${update_by}, update_date=now(), is_delete=true
	WHERE id = ${id};`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: `Data id ${id} is deleted`,
        });
      }
    });
  });

  app.delete("/api/deletedproduct/:id", (req, res) => {
    const id = req.params.id;

    const query = `DELETE FROM public."TblProduct"
	WHERE id = ${id};`;

    pool.query(query, (error, result) => {
      if (error) {
        res.send(500, {
          success: false,
          result: error,
        });
      } else {
        res.send(200, {
          success: true,
          result: `Data id ${id} is deleted`,
        });
      }
    });
  });
};
